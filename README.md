# FabricUI JS

Javascript library for Fabric UI based component rendering.
 
```
var myFabricBar = new FabricCommandBar({
    container: "#homeCommandBar",
    items: [
        {type: "button", text: "New Appointment", icon: {type: "ms-Icon--Add", position: "right"}, align: "left"},
        {type: "button", text: "Edit Selected", icon: "ms-Icon--Edit", align: "left"},
        {type: "search", text: "dugme", align: "right",
            onKeyUp(value, event) {            //first argument is input value and second is event it self
                // myFabricTable.filterDataSource({type: "contains", value: value});
            }
        }
    ],
    onItemClicked(itemData, itemIndex, event) {

    }
});

var myFabricContextMenu = new FabricContextMenu({
    container: "#homeContextualMenu",
    customClass: "test",
    onInitSelect: 2,
    items: [
        {text: "Overview", icon: "ms-Icon ms-Icon--Coffee", hash: "overview"},
        {text: "Appointments", icon: "ms-Icon ms-Icon--Coffee", hash: "appointments"},
        {text: "My Patients", icon: "ms-Icon ms-Icon--Coffee"}
    ],
    onItemClicked(e) {
    }
});

var invoiceBreadcrumb = new FabricBreadcrumb({
    container: "#saleBreadcrumb",
    items: [
        {text: "Receipt", hash: "receipt"},
        {text: "Sale", hash: "saleoverview"}
    ],
    onItemClicked(itemData, itemIndex, event) {
        this.setLevel(itemIndex);
        //gHash.updateHash(itemData.hash);
    }});


var myFabricTable = new FabricTable({
    container: "#homeAppoitmentTable",
    key: "id",
    customClass: "test",
    dataSource: someArray, //"https://jsonplaceholder.typicode.com/photos", //Datasource can be both url and array of objects
    style: "stripped",
    columns: [
        {caption: "Album id", key: "albumId", width: "100px"},
        {caption: "Title", key: "title"},
        {caption: "URL", key: "thumbnailUrl"},
        {caption: "Remove", template(rowIndex, rowData, container){
               var button = document.createElement("div");
               container.appendChild(button);
               var fabricButton = new FabricButton({
                   container: button,
                   text: "Remove",
                   onClick(){
                       myFabricTable.removeFromDataSource(rowIndex);
                   }
               });
            }
        }
    ],
    onRowClick(selectedRows, clickedRow) {

    },
    pageSize: 15,
    //IN PROGRESS
    showBorders: {
        vetical: true,
        horizontal: true
    },
    total: {
        template(numberOfRows) {
            return `There are ${numberOfRows} rows`;
        }
    },
    noDataText: "There is nothing!"
});

var myFabricToast = new FabricToast({
    container: "#toaster"
});

myFabricToast.showToast({
    //customClass: "toastClass"
    title: "Testing toast",
    text: "This is a simple toast for fabric UI",
    duration: 3000, //ako se ne prosledi, ne skida se dok se ne pojavi nov
    position: "bottomright" // or:{top: "200px" , left: "100px"}
});

var signinUsernameInput = new FabricInput({
    container: "#signinUsername",
    type: "text",
    placeholder: "Username",
    name: "username",
    label: {
        text: "Username",
        customCss: "testClass", //Ovako bi izgledalo prosledjivanje klase
        color: "white"
    },
    validation: [{
            type: "required",
            message: "*Please enter Your username"
        }]
});

var myFabricDropdown = new FabricDropdown({
    container: "#dropdownFabric",
    customClass: "testSelectClass",
    dataSource: [{value: 1, display: "jedan"}, {value: 2, display: "dva"}],
    valueExpr: "value",
    displayExpr: "display",
    title: "Select...",
    label: {
        customClass: "testLabelClass",
        text: "This is label"
    },
    onValueChanged(data, event) {
    }
});


var myFabricForm = new FabricForm({
    container: "#fabricForm",
    formData: {orderHeaderShipToCode: "111"},
    items: [
        {
            type: "group",
            title: "Group Title",
            items: [
                {
                    dataField: "first",
                    label: "Test1", //if "label" field is string, label is placed on top of form item
                    editorType: "input"
                },
                {
                    type: "inline",
                    items: [
                        {dataField: "test1", label: "test 1"},
                        {dataField: "test2", label: "test 2"},
                        {dataField: "test3", label: "test 3"}]
                },
                {
                    dataField: "second",
                    editorType: "input",
                    editorOptions: {
                        label: {
                            text: "hello",
                            position: "left" //or "top" (default
                        },
                        onValueChanged(newValue, prevValue) {

                        },
                        validation: [{
                                type: "required",
                                message: "*Please enter unit quantity"
                            }, {
                                type: "numeric",
                                message: "*Must be integer"
                            }]
                    }
                },
                {
                    dataField: "third",
                    editorType: "dropdown",
                    editorOptions: {
                        dataSource: [{value: 1, display: "jedan"}, {value: 2, display: "dva"}],
                        valueExpr: "value",
                        displayExpr: "display",
                        onValueChanged(itemData, event) {

                        }
                    }
                },
                {
                    dataField: "simpleField"
                }
            ]
        },
        {
            type: "group",
            items: [
                {label: "Test12", editorType: "input", dataField: "test12"},
                {label: "Test22", editorType: "input", dataField: "test22"},
                {label: "Test32", editorType: "dropdown", dataField: "test32"}
            ]
        }
    ]
});

var myFabricProgress = new FabricProgress({
    container: "#fabricProgressID",
    items: [
        {
            label: "First Milestone",
            pointText: "1"
        },
        {
            label: "Second Milestone",
            pointText: "2"
        },
        {
            label: "Third Milestone",
            pointText: "3"
        }
    ],
    onItemClicked(itemData, itemIndex, event) {
        this.updateProgress(itemIndex);
    }
});


var myFabricPanel = new FabricPanel({
    container: "#fabricPanel",
    template(parent) {
        return "Welcome to the panelo";
    },
    headerText: "Panelo!"
});

var myFabricPopup = new FabricPopup({
       container: "#fabricPopup" 
    });
    
    myFabricPopup.openPopup({
       title: "Zdravo",
       content: "ovo je neki text",
       forceClose: true,
       actions: [{style: "primary", text:"Accept", onClick(e){console.log(e);}}, {text:"Close", onClick(e){myFabricPopup.closePopup();}}]
    });
```