var pacientData = [
    {id: 1, firstName: "Jelena", lastName: 'Aleksic', email: "jelena@email.com", jmbg: "223136788"},
    {id: 2, firstName: "Martina", lastName: 'Trajkovic', email: "Martina@email.com", jmbg: "213131231231"}
];

var lookupData = [];
for(var j = 0 ; j < 47; j++){
    var obj = {};
    obj.id = j;
    obj.doctorId = Math.floor(Math.random() * 10000) + 1;  
    obj.pacientId = Math.floor(Math.random() * 10000) + 1;  
    obj.date = randomDate(new Date(1970, 10, 10), new Date());
    obj.description = makeid();
    lookupData.push(obj);
}


document.addEventListener('DOMContentLoaded', function () {
    var myFabricBar = new FabricCommandBar({
        container: "#homeCommandBar",
        items: [
            {type: "button", text: "New Appointment", icon: "ms-Icon--Add", align: "left"},
            {type: "button", text: "Edit Selected", icon: "ms-Icon--Edit", align: "left"},
            {type: "search", text: "dugme", align: "right", onItemClick(e){}, onKeyUp(value, keyPressed){
                    myFabricTable.filterDataSource({type:"contains", value: value});
                }}
        ]
    });
    
    var myFabricContextMenu = new FabricContextMenu({
        container: "#homeContextualMenu",
        customClass: "test",
        onInitSelect: 2,
        items: [
            {text: "Overview", icon: "ms-Icon ms-Icon--Coffee"},
            {text: "Appointments", icon: "ms-Icon ms-Icon--Coffee"},
            {text: "My Patients", icon: "ms-Icon ms-Icon--Coffee"}
        ],
        onItemClick(e) {
        }
    });
    
    var myFabricTable = new FabricTable({
        container: "#homeAppoitmentTable",
        key: "id",
        customClass: "test",
        dataSource: lookupData,
        style: "stripped",
        columns: [
            {caption: "id", key: "id"}, {caption: "Doctor ID", key: "doctorId"},{caption: "Date", key: "date"}, {caption: "Desc.", key: "description"}
        ],
        onRowClick(selectedRows, clickedRow){
            
        },
        pageSize:10
    });

});

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 15; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}