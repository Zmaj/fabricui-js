class FabricCommandBar {
    constructor(params) {
        var commandBar = document.createElement("div");
        commandBar.classList.add("ms-CommandBar");
        commandBar.classList.add("ms-CommandBar--navBar");
        var commandBarMainArea = document.createElement("div");
        commandBarMainArea.classList.add("ms-CommandBar-mainArea");
        var commandBarSideArea = document.createElement("div");
        commandBarSideArea.classList.add("ms-CommandBar-sideCommands");
        for (var i = 0; i < params.items.length; i++) {
            var item = params.items[i];
            if (!item.customClass) {
                item.customClass = "customClass";
            }

            var commandBarItem = document.createElement("div");
            switch (item.type) {
                case "search":
                    commandBarItem.classList.add(`ms-SearchBox`);
                    commandBarItem.classList.add(`ms-SearchBox--commandBar`);
                    commandBarItem.innerHTML =
                            `<input class="ms-SearchBox-field" type="text" value="" placeholder="Search...">
                            <label class="ms-SearchBox-label">
                                <i class="ms-SearchBox-icon ms-Icon ms-Icon--Search"></i>
                            </label>`;
                    //For now, we capture only one searchbox (input in command)
                    var searchInput = commandBarItem.firstChild;
                    (function () {
                        var itemIndex = i;
                        if (typeof params.items[itemIndex].onKeyUp === "function") {
                            searchInput.addEventListener("keyup", function (e) {
                                params.items[itemIndex].onKeyUp(searchInput.value, e.key);
                            });
                        }
                    })();
                    break;
                case "button":
                    commandBarItem.classList.add(`ms-CommandButton`);
                    commandBarItem.classList.add(`${item.customClass}`);
                    commandBarItem.innerHTML =
                            `
                            <button class="ms-CommandButton-button">
                                <span class="ms-CommandButton-icon ms-fontColor-themePrimary"><i class="ms-Icon ${item.icon}"></i></span> 
                                <span class="ms-CommandButton-label">${item.text}</span> 
                            </button>`;
                    break;
            }
            if (item.align === "left" || !item.align) {
                commandBarMainArea.appendChild(commandBarItem);
            }
            if (item.align === "right") {
                commandBarSideArea.appendChild(commandBarItem);
            }
        }
        commandBar.appendChild(commandBarSideArea);
        commandBar.appendChild(commandBarMainArea);
        var parent = document.querySelector(params.container);
        parent.appendChild(commandBar);
        this.content = commandBar;
    }
}


class FabricContextMenu {
    constructor(params) {
        var contextMenu = document.createElement("ul");
        contextMenu.classList.add("ms-ContextualMenu", "is-opened", "ms-ContextualMenu--hasIcons");
        if (params.customClass) {
            contextMenu.classList.add(params.customClass);
        }

        for (var i = 0; i < params.items.length; i++) {
            var contextMenuListItem = document.createElement("li");
            (function ()
            {
                var itemData = params.items[i];
                contextMenuListItem.addEventListener("click", function () {
                    var listItems = document.querySelectorAll(`${params.container} ul li`);
                    for (var j = 0; j < listItems.length; j++) {
                        listItems[j].classList.remove("contextMenuSelectedItem");
                        listItems[j].firstChild.classList.remove("contextMenuSelectedItemLink");
                    }
                    this.classList.toggle("contextMenuSelectedItem");
                    this.firstChild.classList.toggle("contextMenuSelectedItemLink");
                    if (typeof params.onItemClick === "function") {
                        params.onItemClick(itemData);
                    } else {
                        throw "EJ nije ti to funkcija";
                    }
                });
            })();
            if (params.items[i].customClass) {
                contextMenuListItem.classList.add(params.items[i].customClass);
            }
            contextMenuListItem.classList.add("ms-ContextualMenu-item");
            contextMenuListItem.innerHTML = `<a class="ms-ContextualMenu-link" tabindex="1">${params.items[i].text}</a> 
                        <i class="ms-Icon ${params.items[i].icon}"></i>`;
            if (i === params.onInitSelect) {
                contextMenuListItem.classList.add("contextMenuSelectedItem");
                contextMenuListItem.firstChild.classList.add("contextMenuSelectedItemLink");
            }
            contextMenu.appendChild(contextMenuListItem);
        }
        var parent = document.querySelector(params.container);
        parent.appendChild(contextMenu);
    }
}

class FabricTable {
    constructor(params) {
        this.dataSource = params.dataSource;
        //visible columns. Contain caption->Display Value and key -> Field from data source
        this.columns = params.columns;
        //Style of table (stripped or normal rows)
        this.style = params.style;
        //Element that table gets inserted into
        this.container = document.querySelector(params.container);
        //event on row click.
        this.onRowClick = params.onRowClick;
        //if present, represents how many rows per page are displayed
        this.pageSize = params.pageSize;
        //Selection mode (single or multiple
        this.selection = params.selection;
        //If single row mode is on, returns single row. Otherwise array of selected rows
        this.selectedRows = [];
        //unique key per table row. Needed for differenting selected rows
        this.key = params.key;
        //Current visible page (default)
        this.currentPage = 1;
        //On start, all rows are displayed as they all pass filterValue test
        this.filteredRows = this.dataSource;
        //value for filter search. On start is empty (every row contains it
        this.filterValue = "";

        //Creating table header
        var table = document.createElement("table");
        table.classList.add("ms-Table");
        table.classList.add("fabricTableTable");
        this.table = table;
        if (params.customClass) {
            table.classList.add(params.customClass);
        }
        var thead = document.createElement("thead");
        thead.classList.add("fabricTableThead");
        var tr = document.createElement("tr");
        tr.classList.add("fabricTableTheadTr");
        for (var i = 0; i < params.columns.length; i++) {
            var th = document.createElement("th");
            th.classList.add("fabricTableTheadTh");
            th.setAttribute("unselectable", "on");
            th.setAttribute("onselectstart", "return false");
            th.innerHTML = params.columns[i].caption;
            tr.appendChild(th);
        }
        thead.appendChild(tr);
        table.appendChild(thead);
        if (params.dataSource.length === 0) {
            var noDataInfo = document.createElement("div");
            noDataInfo.innerHTML = "No data to display";
            table.appendChild(noDataInfo);
            var parent = document.querySelector(params.container);
            parent.appendChild(table);
            return;
        }
//Creating table rows
        var tbody = document.createElement("tbody");
        tbody.classList.add("fabricTableTbody");
        this.tbody = tbody;
        //If no pageSize parameter is visible, display all rows at once
        if (!params.pageSize) {
            for (var j = 0; j < params.dataSource.length; j++) {
                //see function addNewRow
                this.addNewRow(params.dataSource[j]);
            }
            //Otherwise, show only needed rows
        } else {
            for (var j = 0; j < Math.min(params.dataSource.length, params.pageSize); j++) {
                this.addNewRow(params.dataSource[j]);
            }
        }
        table.appendChild(tbody);
        var parent = document.querySelector(params.container);
        parent.appendChild(table);
        this.content = table;
        //Creating wrapper for pagging if paging is enabled
        if (params.pageSize) {
            this.createPagging();
        }
    }
    //Function for filtering dataSource. Can be used outside this js file. Keeps track of rows that pass filter test.
    //Two types of filter exist (matches and contains). We need to first do quick search to be able to create pagging and then we do render of the filtered table.
    //params consist of (type: matches || contains),  and (value: String).
    filterDataSource(params) {
        this.filterValue = params.value;
        this.filteredRows = [];
        //First we clear existing body of table
        while (this.tbody.firstChild) {
            this.tbody.removeChild(this.tbody.firstChild);
        }
        //Going trough dataSource and searching for filtered rows;
        for (var i = 0; i < this.dataSource.length; i++) {
            if (params.type === "contains") {
                for (var key in this.dataSource[i]) {
                    if (this.dataSource[i][key].toString().toLowerCase().includes(params.value.toLowerCase())) {
                        //filteredRows will later be used for drawing table rows
                        this.filteredRows.push(this.dataSource[i]);
                        break;
                    }
                }
            }
            if (params.type === "matches") {
                for (var key in this.dataSource[i]) {
                    if (this.dataSource[i][key].toString().toLowerCase() === params.value.toLowerCase()) {
                        this.filteredRows.push(this.dataSource[i]);
                        break;
                    }
                }
            }
        }
        //After filter has finished and we have number of rows, we can create pagging wrapper and start adding rows.
        if (this.pageSize) {
            for (var j = 0; j < Math.min(this.pageSize, this.filteredRows.length); j++) {
                this.addNewRow(this.filteredRows[j]);
            }
            this.createPagging();
        }
    }
    //Function for adding new rows. Also used to mark found keywords in filtered rows.
    addNewRow(rowData) {
        var tbodyTr = document.createElement("tr");
        tbodyTr.classList.add("fabricTableTbodyTr");
        for (var k = 0; k < this.columns.length; k++) {
            var tbodyTd = document.createElement("td");
            tbodyTd.classList.add("fabricTableTbodyTd");
            //checking if dataSource row has columns that pass test for filter.
            if (rowData[this.columns[k].key].toString().includes(this.filterValue)) {
                //Performing regExp search to see part of column that is (case insensitive) same as filterValue
                var foundText = rowData[this.columns[k].key].toString().match(new RegExp(this.filterValue, "i"));
                //Adding extra class on that found part of the value
                tbodyTd.innerHTML = rowData[this.columns[k].key].toString().replace(new RegExp(foundText, "g"), "<span class=fabricTableTbodyTdFound>" + foundText + "</span>");
            } else {
                tbodyTd.innerHTML = rowData[this.columns[k].key];
            }
            tbodyTr.appendChild(tbodyTd);
        }
        var self = this;
        //Event listeners for clicking on row. Clearing existing classes and adding classes to the selected row(s) (depending on the selection type)
        (function () {
            tbodyTr.addEventListener("click", function () {
                var tableRows = document.querySelectorAll(`#${self.container.id} tbody tr`);
                if (self.selection === "single" || !self.selection) {
                    for (var k = 0; k < tableRows.length; k++) {
                        tableRows[k].classList.remove("fabricTableSelectedRow");
                    }
                    this.classList.toggle("fabricTableSelectedRow");
                    if (typeof self.onRowClick === "function") {
                        self.selectedRows = [rowData];
                        self.onRowClick(self.selectedRows);
                    } else {
                        throw "onRowClick has not been passed as function!";
                    }
                } else if (self.selection === "multiple") {
                    var found = false;
                    this.classList.toggle("fabricTableSelectedRow");
                    for (var l = 0; l < self.selectedRows.length; l++) {
                        if (rowData[self.key] === self.selectedRows[l][self.key]) {
                            self.selectedRows.splice(l, 1);
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        self.selectedRows.push(rowData);
                    if (typeof self.onRowClick === "function") {
                        self.onRowClick(self.selectedRows, rowData);
                    } else {
                        throw "onRowClick has not been passed as function!";
                    }
                }
            });
        }
        )();
        this.tbody.appendChild(tbodyTr);
    }
    //Function for creating pagging wrapper. Pagging consist of four buttons (first, previous, next, last) page
    createPagging() {
        this.currentPage = 1;
        var self = this;
        //if there is already pager, remove it and create new one (needed for re render of paging in case of table filter)
        if (this.container.childNodes.length > 1) {
            var prevWrapper = this.container.getElementsByClassName("fabricTablePaggingWrapper")[0];
            this.container.removeChild(prevWrapper);
        }
        var wrapper = document.createElement("div");
        wrapper.classList.add("fabricTablePaggingWrapper");
        var numberOfPages = Math.ceil(this.filteredRows.length / this.pageSize);

        //fistPage
        var firstButton = document.createElement("button");
        firstButton.classList.add("ms-Button");
        var iconFirstButton = document.createElement("i");
        iconFirstButton.classList.add("ms-Icon");
        iconFirstButton.classList.add("ms-Icon--ChevronLeftSmall");
        var spanButtonFirst = document.createElement("span");
        firstButton.setAttribute("firstButton", "");
        spanButtonFirst.classList.add("ms-Button-label");
        spanButtonFirst.appendChild(iconFirstButton);
        firstButton.appendChild(spanButtonFirst);
        wrapper.appendChild(firstButton);

        //previous page
        var previousButton = document.createElement("button");
        previousButton.classList.add("ms-Button");
        var spanButtonPrevious = document.createElement("span");
        spanButtonPrevious.classList.add("ms-Button-label");
        var iconPreviousButton = document.createElement("i");
        iconPreviousButton.classList.add("ms-Icon");
        iconPreviousButton.classList.add("ms-Icon--ChevronLeftMed");
        previousButton.setAttribute("previousButton", "");
        spanButtonPrevious.appendChild(iconPreviousButton);
        previousButton.appendChild(spanButtonPrevious);
        previousButton.disabled = true;
        previousButton.addEventListener("click", function () {
            nextButton.disabled = false;
            self.currentPage--;
            spanCurrentPage.textContent = `${self.currentPage} / ${numberOfPages}`;
            this.disabled = false;
            while (self.tbody.firstChild) {
                self.tbody.removeChild(self.tbody.firstChild);
            }
            //checking number of rows on page so that we can add empty space at the end
            var k = 0;
            for (var j = (self.currentPage - 1) * self.pageSize; (j <= (self.currentPage * self.pageSize) - 1) && j < self.filteredRows.length; j++) {
                self.addNewRow(self.filteredRows[j]);
                k++;
            }
            if (k < self.pageSize) {
                self.addEmptyRow(k);
            }
            if (self.currentPage === 1) {
                this.disabled = true;
            }

        });
        wrapper.appendChild(previousButton);

        //current page
        var spanCurrentPage = document.createElement("span");
        spanCurrentPage.classList.add("fabricTablePaggingCurrentPage");
        spanCurrentPage.textContent = `${self.currentPage} / ${numberOfPages}`;
        wrapper.appendChild(spanCurrentPage);

        //next page
        var nextButton = document.createElement("button");
        nextButton.classList.add("ms-Button");
        var iconNextButton = document.createElement("i");
        iconNextButton.classList.add("ms-Icon");
        iconNextButton.classList.add("ms-Icon--ChevronRightMed");
        var spanButtonNext = document.createElement("span");
        nextButton.setAttribute("nextButton", "");
        if (this.filteredRows.length < this.pageSize) {
            nextButton.disabled = true;
        }
        nextButton.addEventListener("click", function () {
            previousButton.disabled = false;
            this.disabled = false;
            self.currentPage++;
            spanCurrentPage.textContent = `${self.currentPage} / ${numberOfPages}`;
            while (self.tbody.firstChild) {
                self.tbody.removeChild(self.tbody.firstChild);
            }
            var k = 0;
            for (var j = (self.currentPage - 1) * self.pageSize; (j <= (self.currentPage * self.pageSize) - 1) && j < self.filteredRows.length; j++) {
                self.addNewRow(self.filteredRows[j]);
                k++;
            }
            if (k < self.pageSize) {
                self.addEmptyRow(k);
            }
            if (self.currentPage === numberOfPages) {
                this.disabled = true;
            }
        });
        spanButtonNext.classList.add("ms-Button-label");
        spanButtonNext.appendChild(iconNextButton);
        nextButton.appendChild(spanButtonNext);
        wrapper.appendChild(nextButton);

        //last page
        var lastButton = document.createElement("button");
        lastButton.classList.add("ms-Button");
        var iconLastButton = document.createElement("i");
        iconLastButton.classList.add("ms-Icon");
        iconLastButton.classList.add("ms-Icon--ChevronRightSmall");
        var spanButtonLast = document.createElement("span");
        lastButton.setAttribute("lastButton", "");
        spanButtonLast.classList.add("ms-Button-label");
        spanButtonLast.appendChild(iconLastButton);
        lastButton.appendChild(spanButtonLast);
        wrapper.appendChild(lastButton);
        this.container.appendChild(wrapper);
    }
    //Adding row that acts as blank space so that the table has fixed height
    addEmptyRow(rowSpan) {
        var emptyTbodyTr = document.createElement("tr");
        var emptyTbodyTrTd = document.createElement("td");
        emptyTbodyTrTd.style.height = (30 * (this.pageSize - rowSpan)) + "px";
        emptyTbodyTrTd.colSpan = this.columns.length;
        emptyTbodyTr.appendChild(emptyTbodyTrTd);
        this.tbody.appendChild(emptyTbodyTr);
    }
}



